# Lumidi

**Lumidi** stands for ***Lua based MIDI sequencer***

## Dependencies

- **Lua** is used as the scripting language within Lumidi
- **RtMidi** is used for realtime access to MIDI devices
- **ncurses** is used for terminal based HMI

## Install CMake

```bash
sudo apt-get install cmake cmake-qt-gui cmake-data
```