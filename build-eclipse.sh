#!/bin/bash

export SCM_PATH=$(pwd)

echo "Remove old build folder ..."
rm -rf ./../eclipse-build

echo ">>> Create new build folder ..."
mkdir ./../eclipse-build

cd ./../eclipse-build

echo ">>> Initialize CMake Eclipse project ..."
cmake \
    -G "Eclipse CDT4 - Unix Makefiles" \
    -DCMAKE_BUILD_TYPE=DEBUG \
    -DCMAKE_ECLIPSE_VERSION=4.25.0 \
    -DCMAKE_ECLIPSE_RESOURCE_ENCODING=utf8 \
    ${SCM_PATH}

echo ">>> Finished!"