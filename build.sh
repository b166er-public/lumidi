#!/bin/bash

export SCM_PATH=$(pwd)

echo "Remove old build folder ..."
rm -rf ./../build

echo ">>> Create new build folder ..."
mkdir ./../build

cd ./../build

echo ">>> Initialize CMake project ..."
cmake ${SCM_PATH}

echo ">>> Build project ..."
cmake --build .

echo ">>> Finished!"