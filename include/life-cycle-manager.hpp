#pragma once

#include <string>
#include <memory>
#include <atomic>

#include <utils/logger.hpp>
#include <logic/engine.hpp>

namespace lumidi {

class LifeCycleManager 
{

private:
    std::string configuration_script;
    std::shared_ptr<lumidi::utils::Logger> logger;
    std::atomic_bool termination_requested;
    std::shared_ptr<lumidi::logic::Engine> engine;

public:
    LifeCycleManager(
        std::string script,
        std::shared_ptr<lumidi::utils::Logger>& logger
    );

    void execute(void);
    void requestTermination(void);

};

}