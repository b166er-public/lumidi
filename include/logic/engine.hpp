#pragma once

#include <string>
#include <memory>
#include <atomic>

#include <utils/logger.hpp>

#include <lua.h>

typedef lua_State* p_lua_State;

namespace lumidi {
namespace logic {

class Engine {

private:
    p_lua_State lua_state = nullptr;
    std::string configuration_script;
    std::shared_ptr<lumidi::utils::Logger> logger;
    std::atomic_bool termination_requested;

public:
    Engine(
        std::string configuration_script,
        std::shared_ptr<lumidi::utils::Logger>& logger
    );

    enum class EngineInitResult { OK, NOK };
    EngineInitResult init(void);

    int execute(void);

    void request_termination(void);
    void wait_for_termination(void);

};

}
}    