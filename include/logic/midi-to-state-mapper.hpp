#pragma once

#include <memory>

#include <model/channel-state.hpp>
#include <utils/logger.hpp>
#include <utils/midi-devices.hpp>

namespace lumidi {
namespace logic {

class MidiToStateMapper {

public:
    enum class State { ON, OFF };

private:
    std::shared_ptr<lumidi::utils::Logger> logger;
    std::shared_ptr<lumidi::utils::MidiInDevice> device;
    std::shared_ptr<lumidi::model::ChannelState> state;
    State mapper_state;

    static void on_message_receive_callback(
        double ts, 
        std::vector< unsigned char > *message, 
        void *data
    );

public:
    MidiToStateMapper(
        std::shared_ptr<lumidi::utils::MidiInDevice>& device, 
        std::shared_ptr<lumidi::model::ChannelState>& state,
        std::shared_ptr<lumidi::utils::Logger>& logger 
    );

    void start(void);
    void stop(void);
    State get_state(void);

};

}
}
