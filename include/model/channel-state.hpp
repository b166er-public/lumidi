#pragma once

#include <mutex>

namespace lumidi {
namespace model {

constexpr unsigned int KEY_NUMBER = 128;
constexpr unsigned int CONTROLLER_NUMBER = 128;

typedef unsigned int KeyId;
struct KeyState {
    long change_timestamp;
    unsigned int velocity;
    KeyState(long timestamp, unsigned int velocity);
};

typedef unsigned int ControllerId;
struct ControllerState {
    long change_timestamp;
    unsigned int value;
    ControllerState(long timestamp, unsigned int value);
};

class ChannelState {
private:
    std::mutex io_mutex;
    KeyState keys_states[KEY_NUMBER];
    ControllerState controllers_states[CONTROLLER_NUMBER];
public:
    void setKeyState(
        KeyId id, 
        KeyState state );

    void setControllerState(
        ControllerId id, 
        ControllerState state );

    KeyState getKeyState(KeyId id);
    ControllerState getControllerState(ControllerId id);
};

}
}