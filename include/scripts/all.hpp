#pragma once

#include<string>

namespace lumidi {
namespace scripts {

std::string init_script(void);
std::string processing_script(void);

}
}