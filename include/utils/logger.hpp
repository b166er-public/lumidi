#pragma once

#include <string>
#include <mutex>

namespace lumidi {
namespace utils {

class Logger {

private:
    std::mutex io_mutex;
public:
    void info(std::string msg);
    void warning(std::string msg);
    void error(std::string msg);
};

}
}
