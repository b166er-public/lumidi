#pragma once

#include <vector>
#include <string>
#include <optional>
#include <memory>
#include <rtmidi/RtMidi.h>

namespace lumidi {
namespace utils {

struct MidiInDevice {
    unsigned int id;
    std::unique_ptr<RtMidiIn> port;
    MidiInDevice(
        unsigned int id,
        std::unique_ptr<RtMidiIn> port
    );
};

struct MidiOutDevice {
    unsigned int id;
    std::unique_ptr<RtMidiOut> port;
    MidiOutDevice(
        unsigned int id,
        std::unique_ptr<RtMidiOut> port
    );
};

std::optional<MidiInDevice> open_midi_in_device(unsigned int id);
std::optional<MidiOutDevice> open_midi_out_device(unsigned int id);

std::vector<std::string> get_input_device_names(void);

std::vector<std::string> get_output_device_names(void);

std::string get_midi_in_api_display_name(void);
std::string get_midi_out_api_display_name(void);

void print_all_midi_devices(void);

}    
}
