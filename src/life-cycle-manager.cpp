#include <life-cycle-manager.hpp>

#include <chrono>
#include <thread>

namespace lumidi {

LifeCycleManager::LifeCycleManager(
    std::string script,
    std::shared_ptr<lumidi::utils::Logger>& logger)
{
    this->configuration_script = script;
    this->logger = logger;
    this->termination_requested = false;
}

void LifeCycleManager::execute(void)
{
    if(this->configuration_script.empty())
    {
        this->logger->error("Configuration script seems to empty! LCM termination requested!");
    }
    else {
        // Create engine
        this->engine = std::shared_ptr<lumidi::logic::Engine>(
            new lumidi::logic::Engine(
                this->configuration_script,
                this->logger
            )
        );

        // Initialize engine
        if(this->engine->init() == lumidi::logic::Engine::EngineInitResult::NOK) {
            this->logger->error("Stop LCM as engine could not be started!");
            return;
        }

        // Start engine
        this->engine->execute();
        
        // Wait here for termination request
        while(!this->termination_requested) 
        {
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
        }

        // Request engine termination
        this->engine->request_termination();

        // Wait for engine termination
        this->engine->wait_for_termination();
    }
    
}

void LifeCycleManager::requestTermination(void)
{
    this->logger->info("LCM Termination requested by user!");
    this->termination_requested = true;
}

}