#include <logic/engine.hpp>

#include <lauxlib.h>
#include <lualib.h>
#include <scripts/all.hpp>

static p_lua_State prepareVanillaLuaState(void) {
    p_lua_State L= luaL_newstate();
    luaL_openlibs(L); 
    return L;
}

static int run_script(
    p_lua_State L, 
    std::string script,
    lumidi::utils::Logger& logger
) 
{
    int result_code = luaL_dostring(L, script.c_str());
    if(result_code != LUA_OK)
    {
        std::string msg(lua_tostring(L,-1));
        logger.error("Failed execution of script! Reason follows!");
        logger.error(msg);
    }
    return result_code;
}

namespace lumidi {
namespace logic {

Engine::Engine(
    std::string configuration_script,
    std::shared_ptr<lumidi::utils::Logger>& logger
)
{
    this->configuration_script = configuration_script;
    this->logger = logger;

    this->termination_requested = false;
}

Engine::EngineInitResult Engine::init(void)
{
    this->lua_state = prepareVanillaLuaState();

    int lua_error_code = 0;

    this->logger->info("Execute init script ... ");
    lua_error_code = run_script(
        this->lua_state,
        lumidi::scripts::init_script(),
        *this->logger
    );

    if(lua_error_code != LUA_OK) { return Engine::EngineInitResult::NOK; }

    this->logger->info("Execute configuration script ... ");
    lua_error_code = run_script(
        this->lua_state, 
        this->configuration_script,
        *this->logger
    ); 

    if(lua_error_code != LUA_OK) { return Engine::EngineInitResult::NOK; }
    
    return Engine::EngineInitResult::OK;
}

int Engine::execute(void) 
{
    return run_script(
        this->lua_state, 
        lumidi::scripts::processing_script(),
        *this->logger
    );
}

void Engine::request_termination(void)
{
    this->termination_requested = true;
}

void Engine::wait_for_termination(void)
{

}


}
}