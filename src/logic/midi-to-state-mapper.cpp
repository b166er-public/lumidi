#include <logic/midi-to-state-mapper.hpp>

#include <sstream>

namespace lumidi {
namespace logic {

MidiToStateMapper::MidiToStateMapper(
    std::shared_ptr<lumidi::utils::MidiInDevice>& device, 
    std::shared_ptr<lumidi::model::ChannelState>& state,
    std::shared_ptr<lumidi::utils::Logger>& logger
) 
{
    this->logger = logger;
    this->device = device;
    this->state = state;
    this->mapper_state = State::OFF;
}

void MidiToStateMapper::start(void) 
{
    if(this->mapper_state == State::OFF) {
        this->device->port->setCallback(
            MidiToStateMapper::on_message_receive_callback,
            this
        );
        this->mapper_state = State::ON;
        std::stringstream msg;
        msg << "Started MidiToStateMapper for device [ " << this->device->id << " ] !";
        this->logger->info(msg.str());
    } else {
        std::stringstream msg;
        msg << "MidiToStateMapper for device [ " << this->device->id << " ] can not be started as it is already running!";
        this->logger->error(msg.str());
    }
}

void MidiToStateMapper::stop(void) 
{
    if(this->mapper_state == State::ON) {
        this->device->port->cancelCallback();
        this->mapper_state = State::OFF;
        std::stringstream msg;
        msg << "Stoped MidiToStateMapper for device [ " << this->device->id << " ] !";
        this->logger->info(msg.str());
    } else {
        std::stringstream msg;
        msg << "MidiToStateMapper for device [ " << this->device->id << " ] can not be started as it is not stared yet!";
        this->logger->error(msg.str());
    }
}

void MidiToStateMapper::on_message_receive_callback(
        double ts, 
        std::vector< unsigned char > *message, 
        void *data
)
{
    MidiToStateMapper* self = (MidiToStateMapper*) data;
    return;
}

}
}