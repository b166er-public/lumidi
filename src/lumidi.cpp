#include <iostream>
#include <fstream>
#include <string>
#include <sstream> 
#include <csignal>

#include <metadata/banner.hpp>
#include <utils/midi-devices.hpp>
#include <utils/logger.hpp>
#include <life-cycle-manager.hpp>


static std::string loadFile(std::string file_path)
{
	std::stringstream file_content;

	std::ifstream file_stream(file_path);
	if (file_stream.is_open())
	{
		std::string line;
		while ( std::getline (file_stream,line) )
		{
			file_content << line << '\n';
		}
		file_stream.close();

		return file_content.str();
	}
	else 
	{
		return "";
	}
}

static lumidi::LifeCycleManager* _private_lcm_pointer = nullptr;
static std::shared_ptr<lumidi::utils::Logger> _private_logger_ptr;

static void run_life_cycle_manager(std::string configuration_script_path)
{
	std::shared_ptr<lumidi::utils::Logger> logger(new lumidi::utils::Logger());
	logger->info("Load startup script ...");
	std::string configuration_script = loadFile(configuration_script_path);
	logger->info("Configuration script loaded!");

	// Register termination signal handler
	std::signal(
		SIGINT, 
		[](int signal){
			_private_logger_ptr->info("Process received termination signal!");
			if(_private_lcm_pointer) {
				_private_lcm_pointer->requestTermination();
			}
			
    	}
	);



	logger->info("Prepare LCM ...");
	lumidi::LifeCycleManager lcm(
		configuration_script,
		logger
	);
	_private_logger_ptr = logger;
	_private_lcm_pointer = &lcm;

	logger->info("Transfer controll to LCM!");
	lcm.execute();
	logger->info("Received controll back from LCM!");
}

int main(int argc, char** argv){

	std::cout << lumidi::metadata::lumidi_banner() << std::endl;

	switch(argc) {
		case 1:
			lumidi::utils::print_all_midi_devices();
			std::cout << "[ ATTENTION ] To start engine, please pass the path to a configuration script!" << std::endl << std::endl;
			break;
		case 2:
			run_life_cycle_manager(argv[1]);
			break;
		default:
			std::cout << "Lumidi currently supports maximum one argument!" << std::endl;
			break;
	}
	
	std::cout << "Lumidi says goodbye!" << std::endl;
	return 0;
}