#include <metadata/banner.hpp>

#include <sstream>

namespace lumidi {
namespace metadata {

std::string lumidi_banner() {
    std::stringstream result;
    result << "   __                 _     _ _ " << std::endl;
    result << "  / / _   _ _ __ ___ (_) __| (_)" << std::endl;
    result << " / / | | | | '_ ` _ \\| |/ _` | |" << std::endl;
    result << "/ /__| |_| | | | | | | | (_| | |" << std::endl;
    result << "\\____/\\__,_|_| |_| |_|_|\\__,_|_|" << std::endl;
    result << "                                " << std::endl;                                
    return result.str();
}

}
}