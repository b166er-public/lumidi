#include <model/channel-state.hpp>

namespace lumidi {
namespace model {


KeyState::KeyState(long timestamp, unsigned int velocity) {
    this->change_timestamp = timestamp;
    this->velocity = velocity;
}

ControllerState::ControllerState(long timestamp, unsigned int value) {
    this->change_timestamp = timestamp;
    this->value = value;
}

void ChannelState::setKeyState(
    KeyId id, 
    KeyState state ) {
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    this->keys_states[id] = state;
}

void ChannelState::setControllerState(
    ControllerId id, 
    ControllerState state ) {
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    this->controllers_states[id] = state;
}

KeyState ChannelState::getKeyState(KeyId id) {
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    return this->keys_states[id];
}

ControllerState ChannelState::getControllerState(ControllerId id) {
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    return this->controllers_states[id];
}

}
}