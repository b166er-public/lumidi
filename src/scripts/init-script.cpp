#include <scripts/all.hpp>

namespace lumidi {
namespace scripts {

std::string init_script(void)
{
    return R"lua_code( 
        lumidi = {}
        lumidi.bpm = 120
    )lua_code";
}

}
}