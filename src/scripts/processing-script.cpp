#include <scripts/all.hpp>

namespace lumidi {
namespace scripts {

std::string processing_script(void)
{
    return R"lua_code(
        print("Processing started!")    
        print("Processing finished!")    
    )lua_code";
}

}
}