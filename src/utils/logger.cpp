#include <iostream>

#include <utils/logger.hpp>


namespace lumidi {
namespace utils {

void Logger::info(std::string msg)
{
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    std::cout << "[ I ] : " << msg << std::endl;
}

void Logger::warning(std::string msg)
{
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    std::cout << "[ W ] : " << msg << std::endl;
}

void Logger::error(std::string msg)
{
    const std::lock_guard<std::mutex> io_lock(this->io_mutex);
    std::cout << "[ E ] : " << msg << std::endl;
}

}
}