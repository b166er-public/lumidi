#include <utils/midi-devices.hpp>

#include <memory>

namespace lumidi {
namespace utils {

MidiInDevice::MidiInDevice(
    unsigned int id,
    std::unique_ptr<RtMidiIn> port
)
{
    this->id = id;
    this->port = std::move(port);
}

MidiOutDevice::MidiOutDevice(
    unsigned int id,
    std::unique_ptr<RtMidiOut> port
)
{
    this->id = id;
    this->port = std::move(port);
}

std::optional<MidiInDevice> open_midi_in_device(unsigned int id) 
{
    try {
        std::unique_ptr<RtMidiIn> midiin(new RtMidiIn());
        if(midiin) {
            midiin->openPort(id);
            return MidiInDevice{id, std::move(midiin)};
        }
    }
    catch ( RtMidiError &error ) {
        std::cout << "ERROR: Could not open RtMidiIn handler!" << std::endl;
        error.printMessage();
    }

    return std::nullopt;
}

std::optional<MidiOutDevice> open_midi_out_device(unsigned int id)
{
    try {
        std::unique_ptr<RtMidiOut> midiout(new RtMidiOut());
        if(midiout) {
            midiout->openPort(id);
            return MidiOutDevice{id, std::move(midiout)};
        }
    }
    catch ( RtMidiError &error ) {
        std::cout << "ERROR: Could not open RtMidiOut handler!" << std::endl;
        error.printMessage();
    }

    return std::nullopt;
}

std::vector<std::string> get_input_device_names() {
    std::vector<std::string> result;

    try {
        std::unique_ptr<RtMidiIn> midiin(new RtMidiIn());
        if(midiin) {
            unsigned int midi_in_ports = midiin->getPortCount();

            for ( unsigned int p=0; p<midi_in_ports; p++ ) {
                result.push_back(midiin->getPortName(p));
            }
        }
    }
    catch ( RtMidiError &error ) {
        std::cout << "ERROR: Could not open RtMidiIn handler!" << std::endl;
        error.printMessage();
    }

    return result;
}

std::vector<std::string> get_output_device_names() {
    std::vector<std::string> result;

    try {
        std::unique_ptr<RtMidiOut> midiout(new RtMidiOut());
        if(midiout) {
            unsigned int midi_out_ports = midiout->getPortCount();

            for ( unsigned int p=0; p<midi_out_ports; p++ ) {
                result.push_back(midiout->getPortName(p));
            }
        }
    }
    catch ( RtMidiError &error ) {
        std::cout << "ERROR: Could not open RtMidiOut handler!" << std::endl;
        error.printMessage();
    }

    return result;
}

std::string get_midi_in_api_display_name(void) {
    try {
        std::unique_ptr<RtMidiIn> midiin(new RtMidiIn());
        if(midiin) {
            return RtMidiIn::getApiDisplayName( midiin->getCurrentApi() );
        }
    }
    catch ( RtMidiError &error ) {
        std::cout << "ERROR: Could not open RtMidiIn handler!" << std::endl;
        error.printMessage();
    }

    return "";
}

std::string get_midi_out_api_display_name(void) {
    try {
        std::unique_ptr<RtMidiOut> midiout(new RtMidiOut());
        if(midiout) {
            return RtMidiOut::getApiDisplayName( midiout->getCurrentApi() );
        }
    }
    catch ( RtMidiError &error ) {
        std::cout << "ERROR: Could not open RtMidiOut handler!" << std::endl;
        error.printMessage();
    }

    return "";
}

void print_all_midi_devices(void) {
	std::cout << "Lumidi sees following " << get_midi_in_api_display_name() << " MIDI IN devices:" << std::endl;
	std::cout << std::endl;

	auto midi_in_list = get_input_device_names();
	for(int dev_in = 0; dev_in < midi_in_list.size(); dev_in++) {
		std::cout << "[" << dev_in << "] -> " << midi_in_list[dev_in] << std::endl;
	}

	std::cout << std::endl;
    std::cout << "Lumidi sees following " << get_midi_out_api_display_name() << " MIDI OUT devices:" << std::endl;
	std::cout << std::endl;

	auto midi_out_list = get_output_device_names();
	for(int dev_out = 0; dev_out < midi_out_list.size(); dev_out++) {
		std::cout << "[" << dev_out << "] -> " << midi_out_list[dev_out] << std::endl;
	}

	std::cout << std::endl;
}

}    
}